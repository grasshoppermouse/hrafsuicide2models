
logit.inv = function(x) exp(x)/(1+exp(x))

#' PCbiplot
#' 
#' Plots prcomp object
#' 
PCbiplot <- function(PC, x="PC1", y="PC2", c=1) {
    # PC being a prcomp object
    data <- data.frame(obsnames=row.names(PC$x), PC$x, Score=c)
    plot <- ggplot(data, aes_string(x=x, y=y))
    plot <- plot + geom_point(alpha=.5, size=5, aes(colour=Score))#, geom_text(alpha=.4, size=3, aes(label=obsnames))
    plot <- plot + geom_point(colour='black', shape=1, size=5)
    plot <- plot + scale_colour_gradient2(low="red", high="blue")
    plot <- plot + geom_hline(aes(0), size=.2) + geom_vline(aes(0), size=.2)
    datapc <- data.frame(varnames=rownames(PC$rotation), PC$rotation)
    mult <- min(
        (max(data[,y]) - min(data[,y])/(max(datapc[,y])-min(datapc[,y]))),
        (max(data[,x]) - min(data[,x])/(max(datapc[,x])-min(datapc[,x])))
    )
    datapc <- transform(datapc,
                        v1 = .7 * mult * (get(x)),
                        v2 = .7 * mult * (get(y))
    )
    
    datapc=datapc[sqrt(datapc$v1^2 + datapc$v2^2)>1,]
    
    plot <- plot + coord_equal() + geom_text(data=datapc, aes(x=v1, y=v2, label=varnames), size = 5, vjust=1, color="red")
    plot <- plot + geom_segment(data=datapc, aes(x=0, y=0, xend=v1, yend=v2), arrow=arrow(length=unit(0.2,"cm")), alpha=0.75, color="red")
    
    plot
}

#						AGREE.COEFF2.R
#					     (March 26, 2014)
# Description: This script file contains a series of R functions for computing various agreement coefficients
#		  for 2 raters when the input data file is in the form of 2x2 contingency table showing the distribution
#             of subjects by rater, and by category.
# Author: Kilem L. Gwet, Ph.D.
#


#' kappa2.table
#' 
#' Cohen's kappa (Cohen 1960) coefficient and its standard error for 2 raters when input dataset is a contingency table 
#'
#' The input data "ratings" is a qxq contingency table showing the distribution of
#' subjects by rater, when q is the number of categories.
#'
#' Author: Kilem L. Gwet, Ph.D.
#'
kappa2.table <- function(ratings,weights=diag(ncol(ratings)),conflev=0.95,N=Inf,print=TRUE){
    if(dim(ratings)[1] != dim(ratings)[2]){
        stop('The contingency table should have the same number of rows and columns!') 
    }
    n <- sum(ratings) # number of subjects
    f <- n/N # final population correction  
    q <- ncol(ratings) # number of categories 
    pa <- sum(weights * ratings/n) # percent agreement
    
    pk. <- (ratings%*%rep(1,q))/n
    p.l <- t((t(rep(1,q))%*%ratings)/n)
    pe <- sum(weights*(pk.%*%t(p.l)))
    kappa <- (pa - pe)/(1 - pe) # weighted kappa
    
    # 2 raters special case variance
    
    pkl <- ratings/n
    pb.k <- weights %*% p.l
    pbl. <- t(weights) %*% pk.
    sum1 <- 0
    for(k in 1:q){
        for(l in 1:q){
            sum1 <- sum1 + pkl[k,l]* (weights[k,l]-(1-kappa)*(pb.k[k] + pbl.[l]))^2
        }
    }
    var.kappa <- ((1-f)/(n*(1-pe)^2)) * (sum1 - (pa-2*(1-kappa)*pe)^2)
    stderr <- sqrt(var.kappa)# kappa standard error
    p.value <- 2*(1-pt(kappa/stderr,n-1))
    
    lcb <- kappa - stderr*qt(1-(1-conflev)/2,n-1) # lower confidence bound
    ucb <- min(1,kappa + stderr*qt(1-(1-conflev)/2,n-1)) # upper confidence bound
    if(print==TRUE){
        cat("Cohen's Kappa Coefficient\n")
        cat('=========================\n')	
        cat('Percent agreement:',pa,'Percent chance agreement:',pe,'\n')
        cat('Kappa coefficient:',kappa,'Standard error:',stderr,'\n')
        cat(conflev*100,'% Confidence Interval: (',lcb,',',ucb,')\n')
        cat('P-value: ',p.value,'\n')
    }
    invisible(c(pa,pe,kappa,stderr,p.value))
}



#' scott2.table
#' 
#' Scott's pi coefficient (Scott 1955) and its standard error for 2 raters when input dataset is a contingency table 
#'
#' The input data "ratings" is a qxq contingency table showing the distribution of
#' subjects by rater, when q is the number of categories.
#' 
#' Author: Kilem L. Gwet, Ph.D.
#' 
scott2.table <- function(ratings,weights=diag(ncol(ratings)),conflev=0.95,N=Inf,print=TRUE){
    if(dim(ratings)[1] != dim(ratings)[2]){
        stop('The contingency table should have the same number of rows and columns!') 
    }
    n <- sum(ratings) # number of subjects
    f <- n/N # final population correction  
    q <- ncol(ratings) # number of categories 
    pa <- sum(weights * ratings/n) # percent agreement
    
    pk. <- (ratings%*%rep(1,q))/n
    p.l <- t((t(rep(1,q))%*%ratings)/n)
    pi.k <- (pk.+p.l)/2
    pe <- sum(weights*(pi.k%*%t(pi.k)))
    scott <- (pa - pe)/(1 - pe) # weighted scott's pi coefficint
    
    # 2 raters special case variance
    
    pkl <- ratings/n	     #p_{kl}	
    pb.k <- weights %*% p.l    #\ov{p}_{+k}
    pbl. <- t(weights) %*% pk. #\ov{p}_{l+}
    pbk  <- (pb.k + pbl.)/2    #\ov{p}_{k}
    sum1 <- 0
    for(k in 1:q){
        for(l in 1:q){
            sum1 <- sum1 + pkl[k,l] * (weights[k,l]-(1-scott)*(pbk[k] + pbk[l]))^2
        }
    }
    var.scott <- ((1-f)/(n*(1-pe)^2)) * (sum1 - (pa-2*(1-scott)*pe)^2)
    stderr <- sqrt(var.scott)# Scott's standard error
    p.value <- 2*(1-pt(scott/stderr,n-1))
    
    lcb <- scott - stderr*qt(1-(1-conflev)/2,n-1) # lower confidence bound
    ucb <- min(1,scott + stderr*qt(1-(1-conflev)/2,n-1)) # upper confidence bound
    if(print==TRUE){
        cat("Scott's Pi Coefficient\n")
        cat('======================\n')	
        cat('Percent agreement:',pa,'Percent chance agreement:',pe,'\n')
        cat('Scott coefficient:',scott,'Standard error:',stderr,'\n')
        cat(conflev*100,'% Confidence Interval: (',lcb,',',ucb,')\n')
        cat('P-value: ',p.value,'\n')
    }
    invisible(c(pa,pe,scott,stderr,p.value))
}

#' gwet.ac1.table
#' 
#' Gwet's AC1/Ac2 coefficient (Gwet 2008) and its standard error for 2 raters when input dataset is a contingency table 
#'
#' The input data "ratings" is a qxq contingency table showing the distribution of
#' subjects by rater, when q is the number of categories.
#' 
#' Author: Kilem L. Gwet, Ph.D.
#'
gwet.ac1.table <- function(ratings,weights=diag(ncol(ratings)),conflev=0.95,N=Inf,print=TRUE){
    if(dim(ratings)[1] != dim(ratings)[2]){
        stop('The contingency table should have the same number of rows and columns!') 
    }
    n <- sum(ratings) # number of subjects
    f <- n/N # final population correction  
    q <- ncol(ratings) # number of categories 
    pa <- sum(weights * ratings/n) # percent agreement
    
    pk. <- (ratings%*%rep(1,q))/n
    p.l <- t((t(rep(1,q))%*%ratings)/n)
    pi.k <- (pk.+p.l)/2
    tw <- sum(weights)
    pe <- tw * sum(pi.k *(1-pi.k))/(q*(q-1))
    gwet.ac1 <- (pa - pe)/(1 - pe) # gwet's ac1/ac2 coefficint
    
    # calculation of variance - standard error - confidence interval - p-value
    
    pkl <- ratings/n	     #p_{kl}	
    sum1 <- 0
    for(k in 1:q){
        for(l in 1:q){
            sum1 <- sum1 + pkl[k,l] * (weights[k,l]-2*(1-gwet.ac1)*tw*(1-(pi.k[k] + pi.k[l])/2)/(q*(q-1)))^2
        }
    }
    var.gwet <- ((1-f)/(n*(1-pe)^2)) * (sum1 - (pa-2*(1-gwet.ac1)*pe)^2)
    stderr <- sqrt(var.gwet)# ac1's standard error
    p.value <- 2*(1-pt(gwet.ac1/stderr,n-1))
    
    lcb <- gwet.ac1 - stderr*qt(1-(1-conflev)/2,n-1) # lower confidence bound
    ucb <- min(1,gwet.ac1 + stderr*qt(1-(1-conflev)/2,n-1)) # upper confidence bound
    if(print==TRUE){
        cat("Gwet's AC1/AC2 Coefficient\n")
        cat('==========================\n')	
        cat('Percent agreement:',pa,'Percent chance agreement:',pe,'\n')
        cat('AC1/AC2 coefficient:',gwet.ac1,'Standard error:',stderr,'\n')
        cat(conflev*100,'% Confidence Interval: (',lcb,',',ucb,')\n')
        cat('P-value: ',p.value,'\n')
    }
    invisible(c(pa,pe,gwet.ac1,stderr,p.value))
}



#' bp2.table
#' 
#' Brennan-Prediger coefficient (Brennan & Prediger 1981) and its standard error for 2 raters when input dataset is a contingency table 
#'
#' The input data "ratings" is a qxq contingency table showing the distribution of
#' subjects by rater, when q is the number of categories.
#' 
#' Author: Kilem L. Gwet, Ph.D.
#' 
bp2.table <- function(ratings,weights=diag(ncol(ratings)),conflev=0.95,N=Inf,print=TRUE){
    if(dim(ratings)[1] != dim(ratings)[2]){
        stop('The contingency table should have the same number of rows and columns!') 
    }
    n <- sum(ratings) # number of subjects
    f <- n/N # final population correction  
    q <- ncol(ratings) # number of categories 
    pa <- sum(weights * ratings/n) # percent agreement
    
    tw <- sum(weights)
    pe <- tw/(q^2)
    bp.coeff <- (pa - pe)/(1 - pe) # Brennan-Prediger coefficint
    
    # calculation of variance - standard error - confidence interval - p-value
    
    pkl <- ratings/n	     #p_{kl}	
    sum1 <- 0
    for(k in 1:q){
        for(l in 1:q){
            sum1 <- sum1 + pkl[k,l] * weights[k,l]^2
        }
    }
    var.bp <- ((1-f)/(n*(1-pe)^2)) * (sum1 - pa^2)
    stderr <- sqrt(var.bp)# bp's standard error
    p.value <- 2*(1-pt(bp.coeff/stderr,n-1))
    
    lcb <- bp.coeff - stderr*qt(1-(1-conflev)/2,n-1) # lower confidence bound
    ucb <- min(1,bp.coeff + stderr*qt(1-(1-conflev)/2,n-1)) # upper confidence bound
    if(print==TRUE){
        cat("Brennan-Prediger Coefficient\n")
        cat('============================\n')	
        cat('Percent agreement:',pa,'Percent chance agreement:',pe,'\n')
        cat('B-P coefficient:',bp.coeff,'Standard error:',stderr,'\n')
        cat(conflev*100,'% Confidence Interval: (',lcb,',',ucb,')\n')
        cat('P-value: ',p.value,'\n')
    }
    invisible(c(pa,pe,bp.coeff,stderr,p.value))
}

#' krippen2.table
#' 
#' Scott's pi coefficient (Scott 1955) and its standard error for 2 raters when input dataset is a contingency table 
#'
#' The input data "ratings" is a qxq contingency table showing the distribution of
#' subjects by rater, when q is the number of categories.
#' 
#' Author: Kilem L. Gwet, Ph.D.
#' 
krippen2.table <- function(ratings,weights=diag(ncol(ratings)),conflev=0.95,N=Inf,print=TRUE){
    if(dim(ratings)[1] != dim(ratings)[2]){
        stop('The contingency table should have the same number of rows and columns!') 
    }
    n <- sum(ratings) # number of subjects
    f <- n/N # final population correction  
    q <- ncol(ratings) # number of categories 
    epsi = 1/(2*n)
    pa0 <- sum(weights * ratings/n)
    pa <- (1-epsi)*pa0 + epsi # percent agreement
    
    pk. <- (ratings%*%rep(1,q))/n
    p.l <- t((t(rep(1,q))%*%ratings)/n)
    pi.k <- (pk.+p.l)/2
    pe <- sum(weights*(pi.k%*%t(pi.k)))
    kripp.coeff <- (pa - pe)/(1 - pe) # weighted Krippen's alpha coefficint
    
    # calculating variance
    
    pkl <- ratings/n	     #p_{kl}	
    pb.k <- weights %*% p.l    #\ov{p}_{+k}
    pbl. <- t(weights) %*% pk. #\ov{p}_{l+}
    pbk  <- (pb.k + pbl.)/2    #\ov{p}_{k}
    sum1 <- 0
    for(k in 1:q){
        for(l in 1:q){
            sum1 <- sum1 + pkl[k,l] * ((1-epsi)*weights[k,l]-(1-kripp.coeff)*(pbk[k] + pbk[l]))^2
        }
    }
    var.kripp <- ((1-f)/(n*(1-pe)^2)) * (sum1 - ((1-epsi)*pa0-2*(1-kripp.coeff)*pe)^2)
    stderr <- sqrt(var.kripp)# Kripp. alpha's standard error
    p.value <- 2*(1-pt(kripp.coeff/stderr,n-1))
    
    lcb <- kripp.coeff - stderr*qt(1-(1-conflev)/2,n-1) # lower confidence bound
    ucb <- min(1,kripp.coeff + stderr*qt(1-(1-conflev)/2,n-1)) # upper confidence bound
    if(print==TRUE){
        cat("Krippendorff's alpha Coefficient\n")
        cat('================================\n')	
        cat('Percent agreement:',pa,'Percent chance agreement:',pe,'\n')
        cat('Alpha coefficient:',kripp.coeff,'Standard error:',stderr,'\n')
        cat(conflev*100,'% Confidence Interval: (',lcb,',',ucb,')\n')
        cat('P-value: ',p.value,'\n')
    }
    invisible(c(pa,pe,kripp.coeff,stderr,p.value))
}

#' Cluster bootstrap
#' 
#' \url{http://biostat.mc.vanderbilt.edu/wiki/Main/HowToBootstrapCorrelatedData}
#'
resample <- function(dat, cluster, replace) {
    
    # exit early for trivial data
    if(nrow(dat) == 1 || all(replace==FALSE))
        return(dat)
    
    # sample the clustering factor
    cls <- sample(unique(dat[[cluster[1]]]), replace=replace[1])
    
    # subset on the sampled clustering factors
    sub <- lapply(cls, function(b) subset(dat, dat[[cluster[1]]]==b))
    
    # sample lower levels of hierarchy (if any)
    if(length(cluster) > 1)
        sub <- lapply(sub, resample, cluster=cluster[-1], replace=replace[-1])
    
    # join and return samples
    do.call(rbind, sub)
    
}